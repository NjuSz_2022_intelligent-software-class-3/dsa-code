// KthLargest.cpp
// neonscape @ 20231107
#include <vector>
#include <iostream>
#include <random>
using namespace std;
void swap(vector<int>& p, int a, int b)
{
	int temp = p[a];
	p[a] = p[b];
	p[b] = temp;
}
int rand_range(int l, int r)
{
	// this method was found from C++ 11 standards.
	random_device rand_dev;
	mt19937 gen(rand_dev());
	uniform_int_distribution<int> dis(l, r);
	return dis(gen);
}
int randomPartition(vector<int>& p, int l, int r)
{
	// randomly select a pivot in range [l, r].
	int pivot = rand_range(l, r);
	int val = p[pivot];
	swap(p, pivot, r);
	int id = l;
	for (int pt = l; pt < r; pt++)
	{
		if (p[pt] < val)
		{
			swap(p, pt, id);
			id++;
		}
	}
	swap(p, r, id);
	return id;
}
int kthElement(vector<int>& p, int k, int l, int r)
{
	if (l == r) return p[r];
	int pivot = randomPartition(p, l, r);
	// cout << pivot << endl;
	// print(p);
	if (pivot == k) return p[pivot];
	if (pivot < k) return kthElement(p, k, pivot + 1, r);
	if (pivot > k)return kthElement(p, k, l, pivot - 1);
}
int k;
vector<int> buf;
int main()
{
	cin >> k;
	int temp;
	while (cin >> temp)
	{
		buf.push_back(temp);
		if (cin.get() == '\n')break;
	}
	cout << kthElement(buf, buf.size() - k, 0, buf.size() - 1) << endl;
	return 0;
}