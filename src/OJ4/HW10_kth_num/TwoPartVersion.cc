#include<iostream>
#include<vector>
using namespace std;
// find k_th_Num
int parition(vector<int> &nums, int left, int right) {
    int i = left - 1;
    for (int j = left;j <= right;j++) {
        if (nums[j] >= nums[right]) {
            int temp = nums[j];
            nums[j] = nums[++i];
            nums[i] = temp;
        }
    }
    return i;
}

int quickSelect(vector<int> &nums, int left, int right, int k) {
    int p = parition(nums, left, right);
    if (p == k) {
        return nums[p];
    }
    else if (p > k) {
        return quickSelect(nums, left, p - 1, k);
    }
    else {
        return quickSelect(nums, p + 1, right, k);
    }
}


int main() {
    int k;
    cin >> k;
    vector<int>nums(1000006, 0);
    int temp;
    int len = 0;
    while (cin >> temp) {
        nums[len++] = temp;
        if (cin.peek() == '\n') break;
    }
    cout << quickSelect(nums, 0, len - 1, k - 1);
}