/// @author Mojo
#include <bits/stdc++.h>

using namespace std;

class Solution {
    /**
     * @param nums 待查找的数组
     * @param l 查找的起点(include)
     * @param r 查找的终点(exclude)
    */

    //选定一个pivot并将l到r之间的元素通过pivot划分成三部分，然后返回值等于nums[pivot]的左右两个下标
    pair<int, int> _partition(vector<int> &nums, int l, int r) {
        int p = rand() % (r - l) + l;//随机数
        int target = nums[p];
        int i = l, j = r;
        p = l;
        while (p < j) { // [l,i)<t; [i,p)==t; [p,j) not visited; [j,r)>t
            if (nums[p] < target) swap(nums[i++], nums[p++]);
            else if (nums[p] == target) p++;
            else swap(nums[p], nums[--j]);
        }
        return pair<int, int>(i, j - 1);
    }
public:
    /**
     * @param nums:数组
     * @param k: 第k大
     * @return 第k大的数
     */
    int findKthLargest(vector<int> &nums, int k) {
        k = nums.size() - k;
        int l = 0, r = nums.size();
        while (l < r) { // 左闭右开
            pair<int, int> p = _partition(nums, l, r);// l<= p1 <= p2 < r
            if (p.first <= k && k <= p.second) {
                return nums[k];
            }
            else if (p.second < k)
                l = p.second + 1;// l2 > l1
            else //k < p.first
                r = p.first;// r2 < r1
        }
        return -1;
    }
};
int main() {
    srand(time(nullptr));
    vector<int> nums;
    int num, k;
    cin >> k;
    while (scanf("%d", &num) != EOF) {
        nums.push_back(num);
    }
    cout << Solution().findKthLargest(nums, k);
    // system("pause");
    return 0;
}
