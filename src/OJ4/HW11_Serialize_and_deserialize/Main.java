/*** @author Ye Xiong*/
package HW11_Serialize_and_deserialize;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Main {

    public class Node {
        int val;
        Node left;
        Node right;
        public Node(int val) {
            this.val = val;
        }
    }

    private Node Deserialize_Rec(List<String> nodes) {
        Node new_node;
        if (Objects.equals(nodes.get(0), "#")) {
            nodes.remove(0);
            return null;
        }
        else {
            new_node = new Node(Integer.parseInt(nodes.get(0)));
            nodes.remove(0);
            new_node.left = Deserialize_Rec(nodes);
            new_node.right = Deserialize_Rec(nodes);
        }
        return new_node;
    }

    public Node Deserialize(String Seqs) {
        List<String> nodes = new LinkedList<>(Arrays.asList(Seqs.split(",")));
        return Deserialize_Rec(nodes);
    }

    private String Serialize_Rec(Node node, String str){
        if(node==null){
            str +="#,";
        }
        else{
            str += node.val +",";
            str = Serialize_Rec(node.left, str);
            str = Serialize_Rec(node.right, str);
        }
        return str;
    }
    public String Serialize(Node root) {
        String str = Serialize_Rec(root, "");
        return str.substring(0, str.length()-1);
    }

    // public static void main(String[] args) {
	// 	Tools.tools();
	// }
}