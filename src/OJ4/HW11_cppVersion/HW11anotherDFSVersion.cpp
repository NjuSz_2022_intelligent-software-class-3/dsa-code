// Serialize & Deserialize
// neonscape @ 20231107S

#include <iostream>
#include <string>

using namespace std;

// 寇佬采用了x作为空节点的标志,‘/’表示分隔符

struct Node
{
    int val;
    Node *left;
    Node *right;
    Node(int x) : val(x), left(nullptr), right(nullptr) {}
};
int DecodeVal(string seq)
{
    int p = 0, ans = 0;
    // 这里 寇佬自己手搓了stoi
    while (p < seq.length())
    {
        ans *= 10;
        ans += seq[p] - '0';
        p++;
    }
    return ans;
}

int idx = 0;

Node *Deserialize(string seqs)
{
    if (seqs[idx] == 'x')
    {
        idx += 2;
        return nullptr;
    }
    string tmp = "";
    while (seqs[idx] != '/' && idx < seqs.length())
    {
        tmp += seqs[idx];
        idx++;
    }
    idx++;
    Node *cur = new Node(DecodeVal(tmp));
    Node *l = Deserialize(seqs);
    Node *r = Deserialize(seqs);
    cur->left = l;
    cur->right = r;
    return cur;
}

string Serialize(Node *root)
{
    if (root == nullptr)
        return "x/";
    string l = Serialize(root->left);
    string r = Serialize(root->right);
    return to_string(root->val) + "/" + l + r;
}

#include "tools.h"