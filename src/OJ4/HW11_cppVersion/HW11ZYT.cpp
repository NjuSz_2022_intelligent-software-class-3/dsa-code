/// @author Yutian Zhou
#include <iostream>
#include <string>
using namespace std;

struct Node {
    int val;
    Node* left;
    Node* right;
    Node(int x): val(x), left(nullptr), right(nullptr) {}
};

//周佬这里采用了 ascii码 直接把节点的值+1转化为了ASCII, \
不过需要注意的是 这里节点的值的范围需要在ASCII码表示范围内 \
不过这里为了方便，把一个树转化“完全二叉树”的数组表示形式，同学们可以尝试一下层序遍历

void _encode(Node *n, string &s, int index) {
    if(n==nullptr){
        s[index]='\0';
    }else{
        if(2*index+2>=int(s.size())){
            s.resize(2*index+3);
        }
        s[index]=n->val+1;
        _encode(n->left,s,2*index+1);
        _encode(n->right,s,2*index+2);
    }
}
void _decode(Node* n,string& s,int index){
    if(s[2*index+1]=='\0'){//left child
        n->left=nullptr;
    }else{
        n->left=new Node(s[2*index+1]-1);
        _decode(n->left,s,2*index+1);
    }
    if(s[2*index+2]=='\0'){
        n->right=nullptr;
    }else{
        n->right=new Node(s[2*index+2]-1);
        _decode(n->right,s,2*index+2);
    }
}

Node* Deserialize(string seqs) {
    Node* root=new Node(seqs[0]-1);
    _decode(root,seqs,0);
    return root;
}

string Serialize(Node* root) {
    string s;
    s.resize(3);
    _encode(root,s,0);
    return s;
}

//#include "tools.h"
int main(){
    Node* root=new Node(0);
    root->left=new Node(1);
    root->right=new Node(2);
    string s=Serialize(root);
    Node* reroot;
    reroot=Deserialize(s);
}
