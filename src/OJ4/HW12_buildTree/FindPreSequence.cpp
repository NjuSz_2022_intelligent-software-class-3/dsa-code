// FindPreSequence.cpp
//neonscape@20231107
#include <vector>
#include <iostream>
using namespace std;
struct Node {
    int val;
    Node *l, *r;
    Node(int _val) : val(_val), l(nullptr), r(nullptr) {}
    Node(int _val, Node *_l, Node *_r) : val(_val), l(_l), r(_r) {}
};
vector<int> mid, post;
// 这里不使用hash
int find(int val) {
    for (int i = 0; i < mid.size() >> 1; i++) {
        if (mid[i] == val)
            return i;
    }
    return -1;
}
int ptr;
Node *maketree(int l, int r) {
    if (l >= post.size() || l > r || r < 0)
        return nullptr;
    if (l == r) {
        ptr--;
        return new Node(mid[l]);
    }
    int idx = find(post[ptr]);
    ptr--;
    if (idx == -1)
        throw "index out of range";
    Node *cur = new Node(mid[idx]);
    cur->r = maketree(idx + 1, r);
    cur->l = maketree(l, idx - 1);
    return cur;
}
void visit(Node *cur) {
    if (cur == nullptr)
        return;
    cout << cur->val << " ";
    visit(cur->l);
    visit(cur->r);
}
int n;
int main() {
    int temp;
    while (1) {
        cin >> temp;
        if (cin.eof())
            break;
        mid.push_back(temp);
    }
    for (int i = mid.size() >> 1; i < mid.size(); i++) {
        post.push_back(mid[i]);
    }
    ptr = post.size() - 1;
    Node *rt = maketree(0, ptr);
    visit(rt);
    return 0;
}