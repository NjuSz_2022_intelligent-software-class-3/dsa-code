/// @author why
#include<iostream>
#include<vector>
using namespace std;

vector<int>nums;
struct Node {
    int val;
    Node *left;
    Node *right;
    Node(int x) :val(x) { left = nullptr;right = nullptr; }
};
/**
 * @param in... 中序的区间
 * @param back... 后序的区间
*/
Node *getTree(int inleft, int inright, int backleft, int backright) {
    if (inright < inleft) return nullptr;
    int rootVal = nums[backright];
    Node *root = new Node(rootVal);
    int po = inleft;
    for (int i = inleft;i <= inright;i++) {
        if (nums[i] == rootVal) {
            po = i;
            break;
        }
    }
    int backpo = po - inleft + backleft;
    root->left = getTree(inleft, po - 1, backleft, backpo - 1);
    root->right = getTree(po + 1, inright, backpo, backright - 1);
    return root;
}

void preOrder(Node *root) {
    if (!root) return;
    cout << root->val << " ";
    preOrder(root->left);
    preOrder(root->right);
}

int main() {
    int temp;
    while (cin >> temp) nums.push_back(temp);
    preOrder(getTree(0, nums.size() / 2 - 1,
        nums.size() / 2, nums.size() - 1));
}