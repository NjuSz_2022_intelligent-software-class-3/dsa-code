/*** @author Mojo*/
#include <bits/stdc++.h>

using namespace std;

// 打印一个 vector 空格隔开，末尾换行
void print(vector<int> &ns) {
    for (int n : ns) {
        cout << n << " ";
    }
    cout << endl;
}

void ShellSort(vector<int> &nums) {
    //write ur code here.
    int gap, i, j, len = nums.size();
    for (gap = len >> 1; gap > 0; gap >>= 1) {
        for (i = gap; i < len; i++) {
            for (j = i; j >= gap && nums[j-gap] > nums[j]; j -= gap) { swap(nums[j - gap],nums[j]); }
        }
    }
}

int main(int argc, char const *argv[]) {
    /* code */
    string readed;
    getline(cin, readed);
    stringstream nums_stream(readed);
    int num;
    vector<int> nums;
    while (nums_stream >> num) {
        nums.push_back(num);
    }
    ShellSort(nums);
    print(nums);
    return 0;
}