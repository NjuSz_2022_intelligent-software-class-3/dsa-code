/***
 * @author Ye Xiong
 * 注释: Mojo
*/
#include <fstream>
#include <iostream>

using namespace std;

int main(){
    auto map = new int[3125000]{0};
    ifstream fin;
    for(int i=1;i<=60;i++){
        fin.open(to_string(i)+".txt");
        for(int j=0;j<1000000;j++){
            int x;
            fin>>x;
            int pivot = x >> 5;// 效果等价于 pivot = x / 32, 但是位运算更快
            int offset = x & 31;// 效果等价于 offset = x % 32, 但是位运算更快
            map[pivot] |= (1 << offset);
            // 32个元素为一个组 存在int内 \
            这里可以想象成将int作为一个桶, \
            里面的每个2进制位，记录是否 元素(pivot << 5) + offset 是否出现
        }
        fin.close();
    }
    int pos = 0;
    for(int i=0;i<3125000;i++){// i 为 pivot
        for(int j=0;j<32;j++){// j 为 offset
            if((map[i]>>j)&1){// 查看(pivot << 5) + offset元素是否出现 
                pos++;
                int temp = pos / 100000;
                if(pos % 100000==0 && pos!=0){
                    if(temp<=10)
                        cout<<(i<<5)+j<<endl;
                    else //超过1000000
                        return 0;
                }
            }
        }
    }
}