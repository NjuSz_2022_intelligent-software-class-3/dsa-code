// author: neonscape
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<iostream>
using namespace std;
class CircularQueue {
public:
    CircularQueue(int capacity) 
    {
        arr = new int[capacity];
        front_idx = 0;
        back_idx = -1;
        size = 0;
        max_capacity = capacity;
    }
    
    ~CircularQueue() 
    {
        delete[] arr;
    }

    // 入队列，若队列已满则不添加
    void enqueue(int value) 
    {
        if(size >= max_capacity) return;
        back_idx++;
        if(back_idx >= max_capacity) back_idx -= max_capacity;
        arr[back_idx] = value;
        size++;
    }

    // 出队列
    void dequeue() 
    {
        if(!size)return;
        front_idx++;
        size--;
        if(front_idx >= max_capacity)front_idx -= max_capacity;
        //write ur code here.
    }

    // 返回队首元素
    int front() 
    {
        if(!size)return -1;
        return arr[front_idx];
        //write ur code here.
    }

    // 返回队尾元素
    int rear() 
    {
        if(!size)return -1;
        return arr[back_idx];
        //write ur code here.
    }

    // 检查是否为空
    bool isEmpty() 
    {
        if(!size)return true;
        return false;
        //write ur code here.
    }

    // 检查是否已满
    bool isFull() 
    {
        if(size == max_capacity) return true;
        return false;
        //write ur code here.
    }

private:
    int* arr;
    int front_idx;
    int back_idx;
    int size;
    int max_capacity;
};
int n, temp;
string s;
int main()
{
    CircularQueue* q;
    cin >> s;
    cin >> n;
    q = new CircularQueue(n);
    while(1)
    {
        if(cin.eof())break;
        cin >> s;
        if ( s == "enqueue")
        {
            cin >> temp;
            q->enqueue(temp);
        }
        if(s == "dequeue")
        {
            q->dequeue();
        }
        if(s == "front")
        {
            cout << q->front() << endl;
        }
        if(s == "rear")
        {
            cout << q->rear() << endl;
        }
        if(s == "isEmpty")
        {
            cout << q->isEmpty() << endl;
        }
        if(s == "isFull")
        {
            cout << q->isFull() << endl;
        }
    }
    cin >> n;
    return 0;
}
