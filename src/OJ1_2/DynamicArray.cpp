// author: neonscape
#include<cstdio>
#include<cstdlib>
#include<utility>
#include<string>
#include<cstring>
#include<cstdalign>
#include<iostream>
class DynamicArray {
public:
	DynamicArray() {
		_capacity = 10; // 初始容量为10
		_arr = new int[_capacity];
		_currentSize = 0;
	}
	~DynamicArray() {
		delete[] _arr;
	}

	// 向数组末尾添加元素，如果数组已满，扩展容量，扩展为原来的两倍
	void append(int value) {
		if (this->_currentSize >= this->_capacity) {
			this->_capacity <<= 1;
			int *old = this->_arr;
			this->_arr = new int[this->_capacity];
			std::memcpy(this->_arr, old, sizeof(int) * this->_currentSize);
			delete[] old;
			this->_arr[this->_currentSize] = value;
			this->_currentSize++;
			return;
		}
		this->_arr[_currentSize++] = value;
	}

	// 删除指定索引的元素，如果剩余元素不足capacity的一半，将capacity缩小一半，最小容量为10
	void erase(int index) {
		if (this->_currentSize <= index || index < 0) throw "invalid index!";
		if (((this->_currentSize - 1) < (this->_capacity >> 1)) && (this->_capacity > 10))
			// needs shrinking
		{
			this->_capacity >>= 1;
			int *old = this->_arr;
			this->_arr = new int[this->_capacity];
			for (int i = 0; i < this->_currentSize; i++) {
				if (i == index)continue;
				if (i < index) this->_arr[i] = old[i];
				if (i > index) this->_arr[i - 1] = old[i];
			}
			delete[] old;
		}
		else
			// no need for shrinking
		{
			for (int i = index; i < this->_currentSize; i++) {
				this->_arr[i] = this->_arr[i + 1];
			}
		}
		this->_currentSize--;
	}

	// 获取指定索引的元素
	int get(int index) {
		if (index < 0 || index >= this->_currentSize) throw "invalid index!";
		return this->_arr[index];
	}

	// 设置指定索引的元素值
	void set(int index, int value) {
		if (index < 0 || index >= this->_currentSize) throw "invalid index!";
		this->_arr[index] = value;
	}

	// 返回数组当前大小
	int size() {
		return this->_currentSize;
	}

	// 返回数组容量
	int capacity() {
		return this->_capacity;
	}

	// 打印数组的所有元素
	void print() {
		for (int i = 0; i < this->_currentSize; i++) {
			std::cout << this->_arr[i] << ' ';
		}
		std::cout << std::endl;
	}

private:
	int *_arr;
	int _capacity;
	int _currentSize;
};
using namespace std;
DynamicArray *arr;
int main() {
	arr = new DynamicArray();
	string buf = "";
	int temp, temp2;
	while (1) {
		cin >> buf;
		if (cin.eof())break;
		if (buf == "append") {
			cin >> temp;
			arr->append(temp);
		}
		if (buf == "print") {
			arr->print();
		}
		if (buf == "get") {
			cin >> temp;
			cout << arr->get(temp) << endl;
		}
		if (buf == "set") {
			cin >> temp >> temp2;
			arr->set(temp, temp2);
		}
		if (buf == "size") {
			cout << arr->size() << endl;
		}
		if (buf == "capacity") {
			cout << arr->capacity() << endl;
		}
		if (buf == "erase") {
			cin >> temp;
			arr->erase(temp);
		}
	}
	return 0;
}