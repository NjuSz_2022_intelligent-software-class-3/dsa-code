// author: neonscape
#include<iostream>
#include<limits>
using namespace std;
int val = 0;
int check(int t)
{
    if(t == val)    return 0;
    if(t > val)     return 1;
    if(t < val)     return -1;
}
//check已在tools.h中定义，你可以通过check(int)调用
int guessNumber() 
{
    //write ur code here.
    int cur, upper = INT_MAX, lower = INT_MIN;
    int sign = check(cur);
    while(sign != 0)
    {
        sign = check(cur);
        cout << sign << " " << cur << " " << upper << " " << lower << endl;
        if(sign == 1) upper = cur;
        if(sign == -1) lower = cur;
        cur = (upper + lower) >> 1;
    } 
    return cur;
}
int main()
{
    while(!cin.eof())
    {
        cin >> val;
        int res = guessNumber();
        cout << res << endl;
    }
    return 0;
}
