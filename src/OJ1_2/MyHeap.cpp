// author: neonscape
#include <string>


#include <vector>
#include <iostream>
using namespace std;

class MyHeap {
public:
    MyHeap():_size(0) {}
    void swap(vector<int>& arr, int aIndex, int bIndex)
    {
        int t = arr[aIndex];
        arr[aIndex] = arr[bIndex];
        arr[bIndex] = t;
    }
    void push(int val) {
        //write ur code here.
        _size++;
        int cur = _size - 1;
        _arr.push_back(val);
        while(_arr[cur] < _arr[(cur - 1) >> 1] && cur > 0)
        {
            swap(_arr, cur, (cur - 1) >> 1);
            cur = (cur - 1) >> 1;
        }
    }

    void pop() {
        //write ur code here.
        if(!_size)return;
        _size--;
        swap(_arr, 0, _size);
        _arr.erase(_arr.begin() + _size);
        int cur = 0;
        while((_arr[cur] > _arr[(cur << 1) + 1] && (cur << 1) + 1 < _size) || (_arr[cur] > _arr[(cur << 1) + 2] && (cur << 1) + 2 < _size))
        {
            print();
            if(_arr[cur] > _arr[(cur << 1) + 1])
            {
                swap(_arr, cur, (cur << 1) + 1);
                cur = (cur << 1) + 1;
                continue;
            }
            if(_arr[cur] > _arr[(cur << 1) + 2])
            {
                swap(_arr, cur, (cur << 1) + 2);
                cur = (cur << 1) + 2;
                continue;
            }
        }

    }
    int top() {
        //write ur code here.
        if(!_size) return 0;
        return _arr[0];
    }
    void print()
    {
        for (auto it = _arr.begin(); it != _arr.end(); it++)
        {
            cout << *it << " ";
        }
        cout << endl;
    }
    size_t size() {
        //write ur code here.
        return _size;
    }

private:
    vector<int> _arr;
    size_t _size;
};
MyHeap h;
int main()
{
    string str;
    int temp;
    while(!cin.eof())
    {
        cin >> str;
        if(str == "pop") h.pop();
        if(str == "push")
        {
            cin >> temp;
            h.push(temp);
        }
        if (str == "top");
    }
}